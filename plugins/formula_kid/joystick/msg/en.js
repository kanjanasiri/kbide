Blockly.Msg.JOYSTICK_1_POSITION_TITLE = "Joystick 1 Position";
Blockly.Msg.JOYSTICK_1_POSITION_TOOLTIP = "Read Joystick 1 position (-100 to 100, 0 is middle position)";
Blockly.Msg.JOYSTICK_1_POSITION_HELPURL = "";

Blockly.Msg.JOYSTICK_2_POSITION_TITLE = "Joystick 2 Position";
Blockly.Msg.JOYSTICK_2_POSITION_TOOLTIP = "Read Joystick 2 position (-100 to 100, 0 is middle position)";
Blockly.Msg.JOYSTICK_2_POSITION_HELPURL = "";
